
export class Transaction{

    _id:string = "";
    customer:string = "";
    account:string = "";
    transactionType:string = "";
    typeAmount:string = "";
    amount:Number = 0;
    description:string = "";
    dateTime:string = "";
    availableBalance:Number = 0;
    viewDetails:boolean = false;

    constructor(){
        
    }

}