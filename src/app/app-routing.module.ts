import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './layout/auth/auth.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthModule } from './theme/auth/auth.module';
import { DashboardModule } from './theme/dashboard/dashboard.module';

const routes: Routes = [{
  path: '',
  component: DashboardComponent,
  canActivate : [AuthGuardService],
  children: [
    {
      path: '',
      redirectTo: 'app',
      pathMatch: 'full'
    },
    {
      path: 'app',
      loadChildren: () => DashboardModule
    }
  ]
},
{
  path: '',
  component: AuthComponent,
  children: [
    {
      path: 'auth',
      loadChildren: () => AuthModule
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
