import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Transaction } from '../models/Transaction.model';
import { TransactionList } from '../models/TransactionList.model';


import { User } from '../models/User.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private URL:string = "/api";

  constructor(private _http:HttpClient) { }

  login(credentials:any):Observable<any>{
    return this._http.post(`${this.URL}/auth/login`, credentials);
  }

  register(account:any):Observable<any>{
    return this._http.post(`${this.URL}/auth/register`, account);
  }

  getUser():Observable<User>{
    return this._http.get<User>(`${this.URL}/auth/me`).pipe(
      map(data => {
        return data;
      })
    );
  }

  getTransactionsList():Observable<TransactionList>{
    return this._http.get<TransactionList>(`${this.URL}/transactions/list`).pipe(
      map(data => {
        return data;
      })
    );
  }

  deposit(transaction:any):Observable<any>{
    return this._http.post(`${this.URL}/transactions/deposit`, transaction);
  }

  order(transaction:any):Observable<any>{
    return this._http.post(`${this.URL}/transactions/order`, transaction);
  }

  transfer(transaction:any):Observable<any>{
    return this._http.post(`${this.URL}/transactions/transfer`, transaction);
  }

}
