import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SesionService } from './sesion.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private _sesion:SesionService) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string | null = this._sesion.getToken();

    let request = req;

    if (token) {
      request = req.clone({
        setHeaders: {
          token: token
        }
      });
    }

    return next.handle(request);
  }
}
