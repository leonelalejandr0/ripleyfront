import { Injectable } from '@angular/core';
import { User } from '../models/User.model';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

 user:any = null;

  constructor() { 
    this.user = null;
  }

  setToken(token:string){
    localStorage.setItem('token', token);
  }

  getToken():string | null{
    return localStorage.getItem('token');
  }

  cleanToken(){
    localStorage.removeItem('token');
  }


}
