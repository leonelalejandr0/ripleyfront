import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SesionService } from './sesion.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private _sesion:SesionService, private router:Router) { }

  canActivate():boolean{
    const sesion = this._sesion.getToken();
    if(sesion){
      return true;
    }else{
      this.router.navigate(['/auth/login']);
      return false;
    }
    
  }
}
