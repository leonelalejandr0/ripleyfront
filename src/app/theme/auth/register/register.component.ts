import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  account:any = {};
  msg:string = '';
  isSuccess:boolean = false;

  constructor(private _api:ApiService, private _router:Router) { }

  ngOnInit(): void {
  }

  register(){
    this.msg = '';
    this._api.register(this.account).subscribe((result)=>{
      this.msg = result.message;
      this.isSuccess = true;
    }, (error) => {
      this.isSuccess = false;
      this.msg = error.error.message;
    });
  }

}
