import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User.model';
import { ApiService } from 'src/app/services/api.service';
import { SesionService } from 'src/app/services/sesion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials:any;
  error:string = '';

  constructor(private _api:ApiService, private _sesion:SesionService, private _router:Router) {
    this.credentials = {};
  }

  ngOnInit(): void {
  }

  login(){
    this.error = '';
    this._api.login(this.credentials).subscribe( (data) => {
      this._sesion.setToken(data.token);
      this._api.getUser().subscribe( (result:User) => {
        this._sesion.user = result;
        this._router.navigate(['/app/dashboard']);
      });
    }, (error) => {
      this.error = error.error.message;
    });
  }

}
