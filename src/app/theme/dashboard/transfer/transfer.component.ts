import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  transaction:any = {};
  message:string = '';
  isSuccess:boolean = false;

  constructor(private _api:ApiService) { }

  ngOnInit(): void {
  }

  transfer(){
    this.message = '';
    this.isSuccess = false;
    this._api.transfer(this.transaction).subscribe((result) => {
      this.message = result.message;
      this.isSuccess = true;
    }, (error) => {
      this.message = error.error.message;
      this.isSuccess = false;
    });
  }

}
