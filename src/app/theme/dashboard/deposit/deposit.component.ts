import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  transaction:any = {};
  message:string = '';

  constructor(private _api:ApiService) { }

  ngOnInit(): void {
  }

  deposit(){
    this.message = '';
    this._api.deposit(this.transaction).subscribe((result) => {
      this.message = result.message;
    });
  }

}
