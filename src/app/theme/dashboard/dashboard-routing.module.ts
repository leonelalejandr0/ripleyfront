import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DepositComponent } from './deposit/deposit.component';
import { OrderComponent } from './order/order.component';
import { TransferComponent } from './transfer/transfer.component';

const routes: Routes = [
  {
    path : 'dashboard',
    component : DashboardComponent
  },
  {
    path : 'deposit',
    component : DepositComponent
  },
  {
    path : 'transfer',
    component : TransferComponent
  },
  {
    path : 'order',
    component : OrderComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
