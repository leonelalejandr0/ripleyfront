import { Component, OnInit } from '@angular/core';
import { TransactionList } from 'src/app/models/TransactionList.model';
import { ApiService } from 'src/app/services/api.service';
import { SesionService } from 'src/app/services/sesion.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  transactionList:TransactionList;

  constructor(private _api:ApiService, public _sesion:SesionService) { 
    this.transactionList = new TransactionList;
    this.getTransactions();
  }

  ngOnInit(): void {
  }

  getTransactions(){
    this._api.getTransactionsList().subscribe(( results ) => {
      this.transactionList = results;
    });
  }

}
