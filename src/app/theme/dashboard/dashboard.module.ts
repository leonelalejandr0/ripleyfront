import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DepositComponent } from './deposit/deposit.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransferComponent } from './transfer/transfer.component';
import { OrderComponent } from './order/order.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [DepositComponent, DashboardComponent, TransferComponent, OrderComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ]
})
export class DashboardModule { }
