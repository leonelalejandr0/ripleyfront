import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  transaction:any = {};
  message:string = '';
  isSuccess:boolean = false;

  constructor(private _api:ApiService) { }

  ngOnInit(): void {
  }

  order(){
    this.message = '';
    this.isSuccess = false;
    this._api.order(this.transaction).subscribe((result) => {
      this.message = result.message;
      this.isSuccess = true;
    }, (error) => {
      this.message = error.error.message;
      this.isSuccess = false;
    });
    
  }

}
