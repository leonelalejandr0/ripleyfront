import { Component, OnInit } from '@angular/core';
import { Router, RouterLinkActive } from '@angular/router';
import { SesionService } from 'src/app/services/sesion.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {



  constructor(public _sesion:SesionService, private _router:Router) { 
  }

  ngOnInit(): void {
  }

  logout(){
    this._sesion.user = null;
    this._sesion.cleanToken();
    this._router.navigate(['/auth/login']);
  }

}
