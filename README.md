# MiniBancoFront

Proyecto MiniBanco desarrollado con Angular

## Requisitos

- [NodeJS](https://nodejs.org/es/download/)
- [Angular CLI](https://nodejs.org/es/download/)

## Instalación

```bash
git clone git@gitlab.com:leonelalejandr0/ripleyfront.git
```

Deben clonar el proyecto junto donde está el backend [MiniBancoBack](https://gitlab.com/leonelalejandr0/ripleyback), luego en la terminal ejecutar el siguiente comando

```bash
ng build
```

## Uso

Al terminar el build, se debe levantar el backend [MiniBancoBack](https://gitlab.com/leonelalejandr0/ripleyback)


## Licencia
Desarrollado por Leonel Carrasco
